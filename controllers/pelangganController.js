const { ObjectId } = require("mongodb");

class PelangganController {
  // Get all data from Pelanggan collection
  async getAll(req, res) {
    try {
      const pelanggan = req.app.mongo.collection("pelanggan");
      let dataPelanggan = await pelanggan.find({}).toArray();

      // If no data
      if (dataPelanggan.length === 0) {
        return res.status(404).json({
          message: "Data tidak ditemukan",
        });
      }

      // if successful
      return res.status(200).json({
        message: "Data berhasil ditampilkan",
        data: dataPelanggan,
      });
    } catch (e) {
      // if failed
      console.error(e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  } // end Get All

  // Get One Pelanggan data row
  async getOne(req, res) {
    try {
      const pelanggan = req.app.mongo.collection("pelanggan");
      let dataPelanggan = await pelanggan.findOne({
        _id: new ObjectId(req.params.id),
      });

      if (!dataPelanggan) {
        return res.status(404).json({
          message: "Id pelanggan tidak ditemukan",
        });
      }

      return res.status(200).json({
        message: "Berhasil",
        data: dataPelanggan,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  } // end Get One

  async create(req, res) {
    try {
      // insert/create data pelanggan
      const pelanggan = req.app.mongo.collection("pelanggan");
      let createdData = await pelanggan.insertOne({
        nama: req.body.nama,
        alamat: req.body.alamat,
        noTelp: req.body.noTelp,
        image: req.body.image && req.body.image,
      });
      // successful or failed Insert ternary
      return res.status(201).json({
        message: "Success",
        data: createdData.ops[0] ? createdData.ops[0] : "No Data",
      });
    } catch (e) {
      console.error(e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  } // end Create

  // Update data pelanggan
  async update(req, res) {
    try {
      const pelanggan = req.app.mongo.collection("pelanggan");
      // Update data transaksi
      await pelanggan.updateOne(
        {
          _id: new ObjectId(req.params.id),
        },
        {
          $set: {
            nama: req.body.nama,
            alamat: req.body.alamat,
            noTelp: req.body.noTelp,
            image: req.body.image && req.body.image,
          }
        },
      );

      // Find data that was just updated
      let updatedData = await pelanggan.findOne({
        _id: new ObjectId(req.params.id),
      });

      // check if data was updated
      if (!updatedData) {
        return res.status(404).json({
          message: `Data Not Found`,
        });
      }
      // if success
      return res.status(200).json({
        message: "Success",
        data: updatedData,
      });
    } catch (err) {
      // If error
      console.log(err);
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  } // end of Update

  // Delete data pelanggan
  async deletePelanggan(req, res) {
    try {
      const pelanggan = req.app.mongo.collection("pelanggan");
      // Delete data
      let deletePelanggan = await pelanggan.deleteOne({
        _id: new ObjectId(req.params.id),
      });

      console.log(deletePelanggan.deletedCount);
      // if data deleted is null
      if (deletePelanggan.deletedCount === 0) {
        return res.status(404).json({
          message: "Data not found",
        });
      }

      // If success
      return res.status(200).json({
        message: "Success",
      });
    } catch (err) {
      // If error
      console.log(err);
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  } // end of Delete
}

module.exports = new PelangganController();
