const { ObjectId } = require("mongodb");

class BarangController {
   /*---------Get All data from barang----------*/
    async getAll(req, res) {
      try {
        const barang = req.app.mongo.collection("barang");
        let allData = await barang.find({}).toArray();

        if (allData.length === 0) {
          return res.status(404).json({
            message: "Data barang tidak ditemukan",
          });
        }
        return res.status(200).json({
          message: "Data barang selesai ditampilkan",
          data: allData,
        });
      } catch (err) {
        console.log(err);
        return res.status(500).json({
          message: "Internal Server Error",
          error: err,
        });
      }
    }

    /*---------Get One data from barang----------*/
  async getOne(req, res) {
    try {
      const barang = req.app.mongo.collection("barang");
      let dataBarang = await barang.findOne({
        _id: new ObjectId(req.params.id),
      });
      if (!dataBarang) {
        return res.status(404).json({
          message: "Data barang tidak ditemukan",
        });
      }
      return res.status(200).json({
        message: "Data barang selesai ditampilkan",
        data: dataBarang,
      });
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  /*---------Create data from barang----------*/
  async create(req, res) {
    try {
      const barang = req.app.mongo.collection("barang");
      let createdData = await barang.insertOne({
        nama: req.body.nama,
        harga: req.body.harga,
        id_pemasok: req.body.id_pemasok,
        image: req.body.image ? req.body.image : null,
      });

      return res.status(201).json({
        message: "Success",
        data: createdData.ops[0] ? createdData.ops[0] : "No Data",
      });
    } catch (e) {
        console.log(e)
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  /*---------Update data from barang----------*/
  async update(req, res) {
    try {
      const barang = req.app.mongo.collection("barang");
      await barang.updateOne(
        {
          _id: new ObjectId(req.params.id),
        },
        {
          $set: {
            nama: req.body.nama,
            harga: req.body.harga,
            id_pemasok: req.body.id_pemasok,
            image: req.body.image ? req.body.image : null,
          },
        }
      );

      let newData = await barang.findOne({
        _id: new ObjectId(req.params.id),
      });

      if (!newData) {
        return res.status(404).json({
          message: `Pembaruan Data barang gagal`,
        });
      }

      return res.status(200).json({
        message: "Pembaruan Data barang berhasil",
        data: newData,
      });
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  }

  /*---------Delete data from barang----------*/
  async delete(req, res) {
    try {
      const barang = req.app.mongo.collection("barang");
      let deleteBarang = await barang.deleteOne({
        _id: new ObjectId(req.params.id),
      });

      console.log(deleteBarang.deletedCount);
      if (deleteBarang.deleteCount===0) {
        return res.status(404).json({
          message: "Data Barang tidak ditemukan",
        });
      }
      return res.status(200).json({
        message: `Data Barang telah dihapus`,
      });
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new BarangController();
