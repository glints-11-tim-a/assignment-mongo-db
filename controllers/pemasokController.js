const { ObjectId } = require("mongodb");

class PemasokController {
  // Get All data from pemasok
  async getAll(req, res) {
    try {
      const pemasok = req.app.mongo.collection("pemasok");
      let dataPemasok = await pemasok.find({}).toArray();

      if (dataPemasok.length === 0) {
        return res.status(404).json({
          message: "Data Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data: dataPemasok,
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  } // end get ALL

  // Get One data from pemasok
  async getOne(req, res) {
    try {
      const pemasok = req.app.mongo.collection("pemasok");
      let dataPemasok = await pemasok.findOne({
        _id: new ObjectId(req.params.id),
      });
      if (!dataPemasok) {
        return res.status(404).json({
          message: "Data Not Found",
        });
      }

      return res.status(200).json({
        message: "Success",
        data: dataPemasok,
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  } // end get ALL

  // create data for pemasok
  async create(req, res) {
    try {
      const pemasok = req.app.mongo.collection("pemasok");
      let createdData = await pemasok.insertOne({
        nama: req.body.nama,
        alamat: req.body.alamat,
        email: req.body.email,
        no_hp: req.body.no_hp,
        image: req.body.image && req.body.image, //masih bingung
      });

      return res.status(201).json({
        message: "Success",
        data: createdData.ops[0] ? createdData.ops[0] : "No Data",
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    } // end insert
  }

  // update data from transaksi
  async update(req, res) {
    try {
      const pemasok = req.app.mongo.collection("pemasok");
      // Update data transaksi
      await pemasok.updateOne(
        {
          _id: new ObjectId(req.params.id),
        },
        {
          $set: {
            nama: req.body.nama,
            alamat: req.body.alamat,
            email: req.body.email,
            no_hp: req.body.no_hp,
            image: req.body.image && req.body.image,
          },
        }
      );

      // Find data that updated
      let updatedData = await pemasok.findOne({
        _id: new ObjectId(req.params.id),
      });

      // check if the data updated
      if (!updatedData) {
        return res.status(404).json({
          message: `Data Not Found`,
        });
      }

      return res.status(200).json({
        message: "Success",
        data: updatedData,
      });
    } catch (err) {
      // If error
      console.log(err);
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  }

  //delete one pemasok from database
  async delete(req, res) {
    try {
      const pemasok = req.app.mongo.collection("pemasok");
      // Delete data
      let deletePemasok = await pemasok.deleteOne({
        _id: new ObjectId(req.params.id),
      });

      console.log(deletePemasok.deletedCount);
      // If data deleted is null
      if (deletePemasok.deletedCount===0) {
        return res.status(404).json({
          message: "Data Not Found",
        });
      }

      // If success
      return res.status(200).json({
        message: "Success",
      });
    } catch (err) {
      // If error
      console.log(err);
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  }
}

module.exports = new PemasokController();
