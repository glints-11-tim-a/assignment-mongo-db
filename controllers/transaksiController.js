const { ObjectId } = require("mongodb"); //

class TransaksiController {
  // Get All Data
  async getAll(req, res) {
    try {
      const transaksi = req.app.mongo.collection("transaksi");
      let data = await transaksi.find({}).toArray(); // Get all data from transaksi table

      // If no data
      if (data.length === 0) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      // If success
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  // Get One Data
  async getOne(req, res) {
    try {
      const transaksi = req.app.mongo.collection("transaksi");
      // Find one data
      let data = await transaksi.findOne({
        _id: new ObjectId(req.params.id),
      });

      // If no data
      if (!data) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      // If success
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  // Create data
  async create(req, res) {
    try {
      const transaksi = req.app.mongo.collection("transaksi");
      const barang = req.app.mongo.collection("barang");
      const pelanggan = req.app.mongo.collection("pelanggan");

      let findBarang = await barang.findOne({
        _id: new ObjectId(req.body.barang),
      });

      let findPelanggan = await pelanggan.findOne({
        _id: new ObjectId(req.body.pelanggan),
      });

      let total = eval(findBarang.harga) * req.body.jumlah;

      // Insert data transaksi
      let data = await transaksi.insertOne({
        barang: findBarang,
        pelanggan: findPelanggan,
        jumlah: req.body.jumlah,
        total: total,
        image: req.body.image && req.body.image,
      });

      // If success
      return res.status(200).json({
        message: "Success",
        data: data.ops[0],
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  // Update data
  async update(req, res) {
    try {
      const transaksi = req.app.mongo.collection("transaksi");
      const barang = req.app.mongo.collection("barang");
      const pelanggan = req.app.mongo.collection("pelanggan");

      let findBarang = await barang.findOne({
        _id: new ObjectId(req.body.barang),
      });

      let findPelanggan = await pelanggan.findOne({
        _id: new ObjectId(req.body.pelanggan),
      });

      let total = eval(findBarang.harga) * req.body.jumlah;
      // Update data transaksi
      await transaksi.updateOne(
        {
          _id: new ObjectId(req.params.id),
        },
        {
          $set: {
            barang: findBarang,
            pelanggan: findPelanggan,
            jumlah: req.body.jumlah,
            total: total,
            image: req.body.image && req.body.image,
          },
        }
      );

      // Find data that updated
      let data = await transaksi.findOne({
        _id: new ObjectId(req.params.id),
      });

      // If success
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  // Delete Data
  async delete(req, res) {
    try {
      const transaksi = req.app.mongo.collection("transaksi");
      // delete data depends on req.params.id
      let data = await transaksi.deleteOne({
        _id: new ObjectId(req.params.id),
      });

      // If success
      return res.status(200).json({
        message: "Success to delete transaksi",
      });
    } catch (e) {
      // If failed
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new TransaksiController();
