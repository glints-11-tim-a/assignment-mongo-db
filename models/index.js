const { MongoClient } = require("mongodb"); // Import mongodb

// uri of mongodb in our computer for connection
const uri = process.env.MONGO_URI;

// Make new client / connection
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// create middlewares function to pas the connection/client
function setGlobalConnection(req, res, next) {
  req.app.mongo = req.app.get("mongoConnection");
  next();
}

// pass the client before it connected
module.exports = {client,setGlobalConnection};
