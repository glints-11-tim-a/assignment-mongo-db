const express = require("express"); // Import express

// Import validator
const pelangganValidator = require("../middlewares/validators/pelangganValidator");

// Import controller
const pelangganController = require("../controllers/pelangganController");
// import image handler
const { imageUpload } = require("../middlewares/uploads/imageUpload.js");

// Make router
const router = express.Router();

// CRUD
router.get("/", pelangganController.getAll);

// Create data
router.post("/", imageUpload, pelangganValidator.create, pelangganController.create);

// Get One Data
router.get("/:id", pelangganController.getOne);

// Update Data
router.put("/:id", imageUpload, pelangganValidator.update, pelangganController.update);

// Delete One Data
router.delete("/:id", pelangganController.deletePelanggan);

// Export router
module.exports = router;
