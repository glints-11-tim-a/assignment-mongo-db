const express = require("express"); // Import express
const router = express.Router(); // Make router from app
const { imageUpload } = require("../middlewares/uploads/imageUpload.js"); // import image handler

//import controller
const pemasokController = require("../controllers/pemasokController.js");
//import validator
const pemasokValidator = require("../middlewares/validators/pemasokValidator.js");

//router path
router.get("/", pemasokController.getAll);
router.get("/:id", pemasokController.getOne);
router.post(
  "/",
  imageUpload,
  pemasokValidator.create,
  pemasokController.create
);
router.put(
  "/:id",
  imageUpload,
  pemasokValidator.update,
  pemasokController.update
);
router.delete("/:id", pemasokController.delete);

module.exports = router; // Export router
