const express = require("express"); // Import express
const router = express.Router(); // Make router from app
const { imageUpload } = require("../middlewares/uploads/imageUpload.js"); // import image handler

// Import validator
const transaksiValidator = require("../middlewares/validators/transaksiValidator");
// Import controller
const transaksiController = require("../controllers/transaksiController");

// Get All Data
router.get("/", transaksiController.getAll);
// Get One Data
router.get("/:id", transaksiController.getOne);
// Create data
router.post(
  "/",
  imageUpload,
  transaksiValidator.validate,
  transaksiController.create
);
// Update Data
router.put(
  "/:id",
  imageUpload,
  transaksiValidator.validate,
  transaksiController.update
);
// Delete One Data
router.delete("/:id", transaksiController.delete);

// Export router
module.exports = router;
