const express = require("express"); // Import express
const router = express.Router(); // Make router from app
const { imageUpload } = require("../middlewares/uploads/imageUpload.js"); // import image handler

//import controller
const barangController = require("../controllers/barangController.js");
//import validator
const barangValidator = require("../middlewares/validators/barangValidator.js");

//router path
router.get("/", barangController.getAll);
router.get("/:id", barangController.getOne);
router.post("/", imageUpload, barangValidator.create, barangController.create);
router.put("/:id", imageUpload,  barangValidator.update, barangController.update);
router.delete("/:id", barangController.delete);

module.exports = router; // Export router
