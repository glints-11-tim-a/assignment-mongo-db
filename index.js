require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
}); // Config environment
const express = require("express"); // Import Express
const app = express(); // Create app from express

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// import models (not yet connect DB)
let { client, setGlobalConnection } = require("./models");

//routing transaksi
const transaksiRoutes = require("./routes/transaksiRoute.js");
app.use("/transaksi", setGlobalConnection, transaksiRoutes);

//routing barang
const barangRoutes = require("./routes/barangRoute.js");
app.use("/barang", setGlobalConnection, barangRoutes);

//routing pelanggan
const pelangganRoutes = require("./routes/pelangganRoute.js");
app.use("/pelanggan", setGlobalConnection, pelangganRoutes);

//routing pemasok
const pemasokRoutes = require("./routes/pemasokRoute.js");
app.use("/pemasok", setGlobalConnection, pemasokRoutes);

let PORT = 3000;
client
  .connect()
  .then(() => {
    //connecting to Database
    const penjualanDB = client.db("penjualan_development");
    console.log("DB Connected");

    // set connection to req, so we can access globally in route
    app.set("mongoConnection", penjualanDB);

    //run the application after database connected
    app.listen(PORT, () => console.log(`listen to port ${PORT}`)); // make application have port 3000
  })
  .catch((e) => {
    console.log(e);
  });
