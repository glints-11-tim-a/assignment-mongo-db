const { ObjectId } = require("mongodb");
const validator = require("validator"); // Import validator

module.exports.create = async (req, res, next) => {
  try {
    const pelanggan = req.app.mongo.collection("pelanggan");
    let errors = [];
    if (!validator.isAlpha(validator.blacklist(req.body.nama, " "))) {
      errors.push("Nama must only contain alphabets");
    }

    if (!validator.isNumeric(req.body.noTelp)) {
      errors.push("No telp must be a number");
    }

    // If errors length > 0, it will make errors message
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(" and "),
      });
    }

    next();
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      message: "Internal Server Error",
      error: e,
    });
  }
};

module.exports.update = async (req, res, next) => {
  try {
    const pelanggan = req.app.mongo.collection("pelanggan");
    // Find barang and no Telp
    let findData = await pelanggan.findOne({
      _id: new ObjectId(req.params.id),
    });

    // Create errors variable
    let errors = [];
    console.log(findData);
    // If barang not found
    if (!findData) {
      errors.push("Data pelanggan belum terdaftar");
    }

    if (!validator.isAlpha(validator.blacklist(req.body.nama, " "))) {
      errors.push("Nama must only contain alphabets");
    }

    if (!validator.isNumeric(req.body.noTelp)) {
      errors.push("No telp must be a number");
    }
    // If errors length > 0, it will make errors message
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
    next();
  } catch (e) {
    console.log(e);
    return res.status(500).json({
      message: "Internal Server Error",
      error: e,
    });
  }
};
