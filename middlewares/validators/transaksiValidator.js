const validator = require("validator");
const { ObjectId } = require("mongodb");

exports.validate = async (req, res, next) => {
  try {
    const barang = req.app.mongo.collection("barang");
    const pelanggan = req.app.mongo.collection("pelanggan");
    // Get barang and pelanggan
    let findData = await Promise.all([
      barang.findOne({
        _id: new ObjectId(req.body.barang),
      }),
      pelanggan.findOne({
        _id: new ObjectId(req.body.pelanggan),
      }),
    ]);

    // Create errors variable
    let errors = [];

    // If barang not found
    if (!findData[0]) {
      errors.push("Barang Not Found");
    }

    // If pelanggan not found
    if (!findData[1]) {
      errors.push("Pelanggan Not Found");
    }

    if (!validator.isNumeric(req.body.jumlah)) {
      errors.push("Jumlah must be a number");
    }

    // If errors length > 0, it will make errors message
    if (errors.length > 0) {
      // Because bad request
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    // It means that will be go to the next middleware
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e,
    });
  }
};

