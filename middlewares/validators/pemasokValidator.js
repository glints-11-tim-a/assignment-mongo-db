const { ObjectId } = require("mongodb");
const validator = require("validator");

class PemasokValidator {
  async create(req, res, next) {
    try {
      // Create errors variable
      let errors = [];

      //Set form validation rule
      if (!validator.isAlpha(validator.blacklist(req.body.nama, ' '))) {
        errors.push("Nama harus diisi dengan huruf saja");
      }
      if (!validator.isEmail(req.body.email)) {
        errors.push("Format email tidak sesuai");
      }
      if (!validator.isNumeric(req.body.no_hp)) {
        errors.push("No HP harus diisi dengan angka saja");
      }

      // If errors length > 0, it will make errors message
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      next();
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        error: err,
      });
    }
  }

  async update(req, res, next) {
    try {
      const pemasok = req.app.mongo.collection("pemasok");
      // Create errors variable
      let errors = [];

      // check if the data Exist
      let dataPemasok = await pemasok.findOne({
        _id: new ObjectId(req.params.id),
      });
      if (!dataPemasok) {
        errors.push("data not found");
      }

      //Set form validation rule
      if (!validator.isAlpha(validator.blacklist(req.body.nama, ' '))) {
        errors.push("Nama harus diisi dengan huruf saja");
      }
      if (!validator.isEmail(req.body.email)) {
        errors.push("Format email tidak sesuai");
      }
      if (!validator.isNumeric(req.body.no_hp)) {
        errors.push("No HP harus diisi dengan angka saja");
      }

      // If errors length > 0, it will make errors message
      if (errors.length > 0) {
        // Because bad request
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      next();
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        error: err,
      });
    }
  }
}

module.exports = new PemasokValidator();
