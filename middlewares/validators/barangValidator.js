const { ObjectId } = require("mongodb");
const validator = require("validator");

class BarangValidator {
  async create(req, res, next) {
    try {
      let errors = [];

      if (!validator.isAlpha(validator.blacklist(req.body.nama, ' '))) {
        errors.push("Nama harus diisi dengan huruf saja");
      }
      if (!validator.isNumeric(req.body.harga)) {
        errors.push("Harga barang harus diisi dengan angka saja");
      }
      if (!validator.isAlphanumeric(req.body.id_pemasok)) {
        errors.push("ID Pemasok harus diisi dengan angka saja");
      }

      if (errors.length > 0) {
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      next();
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        error: e,
      });
    }
  }

  async update(req, res, next) {
    try {
      const barang = req.app.mongo.collection("barang");
      let errors = [];

      let dataBarang = await barang.findOne({
        _id: new ObjectId(req.params.id),
      });
      if (!dataBarang) {
        errors.push("Data barang tidak ditemukan");
      }

      if (!validator.isAlpha(validator.blacklist(req.body.nama, ' '))) {
        errors.push("Nama harus diisi dengan huruf saja");
      }
      if (!validator.isNumeric(req.body.harga)) {
        errors.push("Harga barang harus diisi dengan angka saja");
      }
      if (!validator.isAlphanumeric(req.body.id_pemasok)) {
        errors.push("ID Pemasok harus diisi dengan angka saja");
      }

      if (errors.length > 0) {
        return res.status(400).json({
          message: errors.join(", "),
        });
      }
      next();
    } catch (e) {
      console.log(e);
      return res.status(500).json({
        error: e,
      });
    }
  }
}

module.exports = new BarangValidator();
